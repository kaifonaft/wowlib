/**
 * Конструктор класса AlkhimElement
 * 
 * @constructor
 * @param {number} id - id
 * @param {string} name - название
 * @param {string} img - путь к изображению
 */
function AlkhimElement(id, name, img){
    this.id = id;
    this.name = name;
    this.img = img;
}
/**
 * @return {Element} dom-представление Алхимического элемента
 */
AlkhimElement.prototype.getDOM = function(){
    var div = $('<div/>',{
        'class': 'AlkhimElement',
        'data-id': this.id,
        'data-name': this.name,
        'data-img': this.img
    });
    var img = $('<img/>', {
        'src':this.img,
        'alt':this.name,
        'title':this.name
    });
    div.append(img);
    return div;
}
/**
 * @param {Object} div JQuery обёртка div-а алхимического элемента
 * @return {AlkhimElement} Объект алхимического элемента*/
AlkhimElement.prototype.createElementFromDiv = function(div){
    var id, name, img;
    id = div.attr('data-id');
    name = div.attr('data-name');
    img = div.attr('data-img');
    return new AlkhimElement(id, name, img);
}
/**
 * @class фабрика основных селекторов
 */
function Factory(){}
/**
 * @return {object} поле в котором хранятся составляющие рецепта */        
Factory.prototype.getInput = function(){
    return $('#ingrInput');
}
/**
 * @return {object} поле для поиска по подстроке среди элементов в allWrap
 */
Factory.prototype.getSearchInput = function(){
    return $('#ingrSearchInput');
}
/**
 * @return {object} div в котором хранится визуальное представление выбранных игредиентов */
Factory.prototype.getSelectWrap = function(){
    return $($('#ingredientSelector .wrapper')[0]);
}
/**
 * @return {object} div для выбираемых элементов
 */
Factory.prototype.getAllWrap = function(){
    return $($('#ingredientSelector .all')[0]);
}
/** @return {object} div для хранения id загруженных ингредиентов */
Factory.prototype.getSelectedIds = function(){
    return $($('#ingredientSelector .selected-ids')[0]);
}
/**
 * добавление и удаление ингредиентов из поля формы
 * @constructor
 * @param {object} input html-элемент input, хранящий значения ингредиентов
 */
function InputController(input){
    this.input = input;
}
/***/
InputController.prototype.addId = function(id){
    var jinput = $(this.input);
    var value = jinput.val();
    if(value == ''){
        value = id;
    }else{
        value += ','+id;
    }
    jinput.val(value);
}
InputController.prototype.deleteId = function(id){
    var jinput = $(this.input);
    var value = jinput.val();
    var reg = new RegExp(','+id+'(,|$)');
    value = ','+value;
    value = value.replace(reg, '$1');
    value = value.substr(1);
    jinput.val(value);
}
var factory;
var inputController;

$(function(){
    // инициализация формы и визуального отображения
    factory = new Factory();
    inputController = new InputController(factory.getInput());
    idsStr = factory.getSelectedIds().text();
    factory.getInput().val(idsStr);
    factory.getSelectWrap().html('');
    if(idsStr!= ''){
        ids = idsStr.split(',');
        for(i in ids){
            div = $('#all_AlkhimElement_'+ids[i]);
            el = AlkhimElement.prototype.createElementFromDiv(div);
            factory.getSelectWrap().append(el.getDOM());
        }
    }
    
    factory.getAllWrap().on('click', 'div.AlkhimElement', null, addIngredient);
    factory.getSelectWrap().on('click', 'div.AlkhimElement', null, delIngredient);
    console.log('before change');
    factory.getSearchInput().keyup(changeSearchIngredient);
    console.log('after change');
});
/**
 * обработка добавления ингредиента к рецепту */
function addIngredient(event){
    var div, ael;
    div = $(event.currentTarget);
    ael = AlkhimElement.prototype.createElementFromDiv(div);
    factory.getSelectWrap().append(ael.getDOM());
    inputController.addId(ael.id);
}
/**
 * обработка удаления ингредиента из рецепта
 */
function delIngredient(event){
    var el = event.currentTarget;
    inputController.deleteId(el.getAttribute('data-id'));
    el.parentNode.removeChild(el);
}
/**
 * отображение результатов поиска по названию алхим. элементов
 */
function changeSearchIngredient(event){
    var input = event.currentTarget;
    var search = input.value.toLowerCase();
    var elements = factory.getAllWrap().children('.AlkhimElement');
    for(var i=0; i<elements.length; i++){
        var name = elements[i].getAttribute('data-name').toLowerCase();
        if(name.indexOf(search) !== -1){
            $(elements[i]).removeClass('notfinded');
        }else{
            $(elements[i]).addClass('notfinded');
        }
    }
}