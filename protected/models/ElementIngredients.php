<?php
/**
 * модель для связи элемента(рецепта) с ингредиентами
 *
 * @author Александр
 */
class ElementIngredients extends CActiveRecord{
    /**
     * @var int id рецепта
     */
    public $recept_id;
    /**
     * @var int id ингредиента
     */
    public $ingredient_id;
    /**
     * @var int количество этого ингредиента в рецепте 
     */
    public $count;
    
    public static function model($classname = __CLASS__){
        return parent::model($classname);
    }
    
    public function tableName() {
        return 'element_ingredients';
    }
}
