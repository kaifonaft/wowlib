<?php
/**
 * класс для выведения в лог(Yii::trace) переменных
 *
 * @author Александр
 */
class Log {
    /**
     * выводит в лог заголовок и значение переменной при помощи метода Yii::trace
     * для массивов и объектов при выводе используется функция print_r
     * @param mixed $var переменная значение которой нужно вывести
     * @param string $label заголовок
     */
    public static function trace($var, $label = ''){
        if($label != ''){
            $print = $label."\n";
        }
        if(is_array($var) || is_object($var)){
            $print .= print_r($var, true);
        }else{
            $print .= $var;
        }
        Yii::trace($print);
    }
}
