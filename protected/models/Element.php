<?php
/**
 * Алхимический элемент
 *
 * @author Александр
 */
class Element extends CActiveRecord{
    
    /**
     * @var string путь к файлу изображения, относительно корня сайта(webroot)
     */
    public $img;
    /**
     * @var CUploadedFile объект для сохранения файла
     */
    public $imgFile;
    /**
     * @var boolean удалять ли изображение
     */
    public $imgDelete;
    /**
     * @var array составляющие рецепта, массив id элементов
     */
    public $ingredientsArray;
    /**
     * @var string составляющие рецепта, список id элементов через запятую. Database
     */
    public $ingredients;
    /**
     * @var array массив. ключ - id ингредиента. значение - количество ингредиента
     */
    public $ingredientsCounts;


    public static function model($className = __CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'element';
    }
    
    public function primaryKey() {
        return 'id';
    }
    
    public function rules() {
        return array(
          array('name, type_id', 'required', 
            'message' => '{attribute} не может быть пустым'),
          array(
            'type_id',
            'in',
            'allowEmpty' => true,
            'range' => $this->getTypeList(),
          ),
          array(
            'imgFile', 'file',
            'types' => 'jpg, png, gif',
            'maxSize' => 5 * pow(2, 20),
            'tooLarge' => '{attribute} максимальный размер файла 5 Мб',
            'allowEmpty' => true,
          ),
          array(
            'ingredientsArray',
            'validateIngredients',
          ),
          array('ingredients', 'safe'),
          array('imgDelete', 'boolean'),
        );
    }
    
    public function relations(){
        return array(
          'type' => array(self::BELONGS_TO, 'AlkhimType', 'type_id'),
          'rel_ingredients' => array(self::MANY_MANY, 'Element', 'element_ingredients(recept_id, ingredient_id)'),
//          'rel_ingredients' => array(self::BELONGS_TO, 'Element', 'element_ingredients(ingredient_id, recept_id)'),
        );
    }
    
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['type_id'] = 'Тип';
        $labels['name'] = 'Название';
        $labels['imgFile'] = 'Изображение';
        $labels['imgDelete'] = 'Удалить изображение';
        $labels['ingredientsArray'] = 'Ингредиенты';
        $labels['ingredients'] = 'Ингредиенты';
        return $labels;
    }
    
    /**
     * проверяет массив id ингредиентов. Существуют ли такие элементы.
     * @param string $attribute имя атрибута
     * @param array дополнительные параметры
     */
    public function validateIngredients($attribute, $params){
        if(!isset($params['message'])){
            $params['message'] = '{attribute} Некорректные элементы: ';
        }
        if(is_array($this->$attribute) && !empty($this->$attribute)){
            $elementIds = Yii::app()->db->createCommand()
                ->select('id')
                ->from('element')
                ->queryColumn();
            $wrongElements = array();
            foreach ($this->$attribute as $elementId){
                if(!in_array($elementId, $elementIds)){
                    $wrongElements []= $elementId;
                }
            }
            if(@$this->id && in_array($this->id, $this->$attribute)){
                $message = $attribute.' Элемент не может являтся своим ингредиентом';
                $this->addError($attribute, $message);
            }
            if(!empty($wrongElements)){
                $params['message'] = preg_replace('/\{attribute\}/', $attribute, $params['message']);
                $message = $params['message'].implode(', ', $wrongElements);
                $this->addError($attribute, $message);
            }
        }
    }
    
    /**
     * @return string ссылка на элемент. В теле ссылки имя элемента
     */
    public function gridGetRefName($data, $row){
        return CHtml::link($data['name'], array('site/view', 'id' => $data['id']));
    }
    
    /**
     * функция для CGridView 
     * @return string отображение ингредиентов в колонке
     */
    public function gridGetIngredients($data, $row){
        return Yii::app()->controller->renderPartial('gridIngredients', $data, true);
    }
    
    /**
     * @param boolean $flagDeleteFile установлен ли флаг удалить файл
     * @param string $lastpathimg путь к изображению относительно корня сайта
     */
    public function saveImg($flagDeleteFile = false, $lastpathimg = ''){
        $imgFile = CUploadedFile::getInstance( $this, 'imgFile');
        if($imgFile){
            $this->deleteImg($lastpathimg);
            $pathfromroot = 'images'.'/'.$this->id.'.'.$imgFile->getExtensionName();
            $path = Yii::getPathOfAlias('webroot').'/'.$pathfromroot;
            $imgFile->saveAs($path);
            $this->img = $pathfromroot;
            $this->save(false);
        }elseif($flagDeleteFile){
            $this->deleteImg($lastpathimg);
            $this->img = null;
            $this->save(false);
        }
    }
    
    /**
     * удаляет изображение
     * @param string $relativepath путь к изображению, относительно корня сайта
     */
    public function deleteImg($relativepath){
        $path = Yii::getPathOfAlias('webroot').'/'.$relativepath;
        if(!$this->isNewRecord && $relativepath != '' && file_exists($path)){
            unlink($path);
        }
    }
    
    protected function beforeValidate() {
        parent::beforeValidate();
        // перевод id-шников ингредиентов из строки в массив
        if(is_string($this->ingredients) && $this->ingredients != ''){
            $this->ingredientsArray = explode(',', $this->ingredients);
        }else{
            $this->ingredientsArray = array();
        }
        return true;
    }

    protected function afterFind() {
        parent::afterFind();
        // строковое представление ингредиентов(id через запятую)
        $this->ingredients = '';
        // массив ингредиентов
        $this->ingredientsArray = array();
        // массив хранящий количества ингредиентов
        $this->ingredientsCounts = array();
        $ingredientsRelRows = ElementIngredients::model()->findAll(
            array(
              'select' => 'ingredient_id,count',
              'condition' => 'recept_id = :recept_id',
              'params' => array('recept_id' => $this->id),
            )
        );
        foreach($ingredientsRelRows as $ingredientRelRow){
            $this->ingredientsCounts[$ingredientRelRow->ingredient_id] = $ingredientRelRow->count;
            for($i=0; $i<$ingredientRelRow->count; $i++){
                $this->ingredientsArray []= $ingredientRelRow->ingredient_id;
                if($this->ingredients != ''){
                    $this->ingredients .= ',';
                }
                $this->ingredients .= $ingredientRelRow->ingredient_id;
            }
        }
    }
    
    protected function afterSave() {
        parent::afterSave();
        // удаление старых ингредиентов
        ElementIngredients::model()->deleteAllByAttributes(array('recept_id' => $this->id));
        // сохранение ингредиентов
        $ingredientCounts = array();
        foreach($this->ingredientsArray as $ingredientId){
            // подсчет числа ингредиентов
            $ingredientCounts[$ingredientId]++;
        }
        foreach($ingredientCounts as $ingredientId => $ingredientCount){
            $relation = new ElementIngredients();
            $relation->recept_id = $this->id;
            $relation->ingredient_id = $ingredientId;
            $relation->count = $ingredientCount;
            $relation->save();
        }
    }

    protected function afterDelete() {
        parent::afterDelete();
        // удаление изображения
        $this->deleteImg($this->img);
        // удаление старых ингредиентов
        ElementIngredients::model()->deleteAll(array(':recept_id' => $this->id));
    }
    
    public function getElementListView(){
        $elementList = Element::model()->findAll();
        $values = array();
        foreach($elementList as $element){
            $values[$element->id] = $element->name;
        }
        return $values;
    }
    
    /**
     * @return array список типов элементов 
     */
    public function getTypeList(){
        $typeList = AlkhimType::model()->findAll();
        $values = array();
        foreach($typeList as $type){
            $values []= $type->id;
        }
        return $values;
    }
    
    /**
     * @return array список типов элементов для вьюхи в формате array(value => name, ...)
     */
    public function getTypeListView(){
        $typeList = AlkhimType::model()->findAll();
        $val2name = array();
        foreach($typeList as $type){
            $val2name[$type->id] = $type->name;
        }
        return $val2name;
    }
    
    /**
     * возвращает DataProvider для CGridView
     * @return CActiveDataProvider dataprovider для поиска
     */
    public function search(){
        $dataProvider = new CActiveDataProvider('Element');
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->params = array();
        $criteria->with = array(
          'type' => array(
            'alias' => 'ttype'
          ),
          'rel_ingredients' => array(
            'alias' => 'relingr',
          ),
        );
        if($this->id){
            $criteria->addCondition("t.id = :id");
            $criteria->params[":id"] = $this->id;
        }
        if($this->name){
            $criteria->addSearchCondition('t.name', $this->name);
        }
        if($this->type_id){
            $criteria->addCondition("t.type_id = :type");
            $criteria->params[":type"] = $this->type_id;
        }
        
        if(!empty($this->ingredientsArray)){
            $command = Yii::app()->db->createCommand();
            // получаем список id рецептов содержащих указанные ингредиенты
            $ids = $command->select('el.id')
                ->from('element el')
                ->join('element_ingredients ingr', 'el.id = ingr.recept_id')
                ->where(array('in', 'ingr.ingredient_id', $this->ingredientsArray))
                ->group('el.id')
                ->queryColumn();
            $criteria->addInCondition('t.id', $ids);
        }
        Log::trace($ids);
        $data = Element::model()->findAll($criteria);
        
        $dataProvider->setCriteria($criteria);
        return $dataProvider;
    }
}
