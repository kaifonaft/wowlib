<?php
/**
 * внутри CGridView ссылки просмотр, редактирование, удаление
 *
 * @author Александр
 */
class MyButtonColumn extends CButtonColumn{
    public $updateButtonUrl='Yii::app()->controller->createUrl("AlkhimEdit",array("id"=>$data->primaryKey))';
   	public $deleteButtonUrl='Yii::app()->controller->createUrl("AlkhimDelete",array("id"=>$data->primaryKey))';
}
