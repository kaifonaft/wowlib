<?php
/**
 * Тип алхимического элемента
 *
 * @author Александр
 */
class AlkhimType extends CActiveRecord{
    
    public static function model($classname = __CLASS__){
        return parent::model($classname);
    }
    
    public function tableName() {
        return 'type';
    }
    
    public function primaryKey() {
        return 'id';
    }
    
    public function rules() {
        return array(
          array(
            'name',
            'length',
            'allowEmpty' => false,
            'max' => 255,
            'tooLong' => '{attribute} максимальная длина: {max}',
          ),
        );
    }
    
    public function relations(){
        return array(
            'element' => array(self::HAS_MANY, 'Element', 'type_id'),
        );
    }
    
}
