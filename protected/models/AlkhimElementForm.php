<?php
/**
 * Элемент - зелье, либо ингредиент.
 *
 * @author Александр
 */
class AlkhimElementForm extends CFormModel{
    public $id;
    public $name;
    public $type;
    public $img;
    
//    public function __con
    
    public function rules(){
        return array(
          array('name, type','required',
            'message' => '{attribute} не может быть пустым',
          ),
          array('img', 'file',
            'maxSize' => 5 * pow(2, 20),
            'allowEmpty' => true,
            'types' => 'jpg, png, gif',
            'tooLarge' => '{attribute} максимальный размер файла 5 Мб',
          ),
          array('name','length',
            'max' => 255,
            'tooLong' => 'Максимально допустимая длина 10',
          ),
          array('type','numerical',
            'integerOnly' => true,
            'allowEmpty' => false,
            'min' => 1,
            'max' => 4,
            'tooBig' => 'Type должно быть целым числом от 1 до 4.',
            'tooSmall' => 'Type должно быть целым числом от 1 до 4.',
          ),
        );
    }
    
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['type'] = 'Тип';
        $labels['name'] = 'Название';
        return $labels;
    }
    
    public function getRow($id){
        return Yii::app()->db->createCommand()
            ->select('id, name, type, img')
            ->from('element')
            ->where('id=:id', array(':id' => $id))
            ->queryRow();
    }
}
