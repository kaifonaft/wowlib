<div class="ingredients">
    <?php foreach($data->rel_ingredients as $ingr): ?>
    <div class="image">
        <?= CHtml::image($ingr->img ? $ingr->img : '', $ingr->name, array('title' => $ingr->name)) ?>
    </div>
    <?php endforeach; ?>
</div>


