<?php /**
 * @var Element $model
 */ ?>
<div class="customview">
    <div class="links">
        <?= CHtml::link('edit', array('site/AlkhimEdit', 'id' => $model->id),
            array('class' => 'edit', 'title' => 'Редактировать')) ?>
        <?= CHtml::linkButton('delete', array(
          'submit' => array(
            'site/AlkhimDelete',
            'id' => $model->id,
          ),
          'confirm' => 'Вы действительно хотите удалить?',
          'class' => 'delete',
          'title' => 'Удалить',
          )) ?>
    </div>
    <div class="name"><?= CHtml::encode($model->name) ?></div>
    <div class="type"><?= CHtml::encode($model->type->name) ?></div>
    <div class="image"><?= CHtml::image($model->img) ?></div>
    <div class="ingredients">
        <?php foreach($model->rel_ingredients as $ingredient): ?>
            <div class="elem">
                <div class="img"><?= CHtml::image($ingredient->img) ?></div>
                <div class="elemname"><?= CHtml::encode($ingredient->name) ?></div>
                <div class="count"><?= CHtml::encode($model->ingredientsCounts[$ingredient->id]) ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
