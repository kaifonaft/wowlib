<?php
/**
 * @var $model Element
 * @author Prigornev Alexander
 */
?>
<h1><?= $model->isNewRecord ? 'Добавить' : 'Сохранить' ?> элемент</h1>
<form action="">
    <input type="hidden" name="id" value="<?= $model->attributes['id'] ?>" />
</form>
<div class="form">
    <div class="message">
        <?= $message ?>
    </div>
    <?= CHtml::beginForm('', 'post', array('enctype' => 'multipart/form-data')) ?>
    <div class="errors">
        <?= CHtml::errorSummary($model) ?>
    </div>
    <div class="row">
        <?= CHtml::activeLabel($model, 'name') ?>
        <?= CHtml::activeTextField($model, 'name') ?>
        <?= CHtml::error($model, 'name') ?>
    </div>
    <div class="row">
        <?= CHtml::activeLabel($model, 'type_id') ?>
        <?= CHtml::activeDropDownList($model, 'type_id', $model->getTypeListView()) ?>
        <?= CHtml::error($model, 'type_id') ?>
    </div>
    <div class="row">
        <?= CHtml::activeLabel($model, 'imgFile') ?>
        <div class="img">
            <?php if(@$model->img): ?>
                <?= CHtml::image($model->img)?>
            <?php endif; ?>
        </div>
        <?= CHtml::activeFileField($model, 'imgFile') ?>
        <?= CHtml::error($model, 'imgFile') ?>
        <?php if($model->img): ?>
        <div class="row checkbox label-inline">
            <?= CHtml::activeCheckBox($model, 'imgDelete') ?>
            <?= CHtml::activeLabel($model, 'imgDelete') ?>
            <?= CHtml::error($model, 'imgDelete') ?>
        </div>
        <?php endif; ?>
    </div>
    <div class="row">
        <?= CHtml::scriptFile('js/ingrEditForm.js') ?>
        <div id="ingredientSelector">
            <?= CHtml::activeLabel($model, 'ingredients') ?>
            <div>
                <input type="text" name='ingrSearch' id='ingrSearchInput' placeholder='поиск по названию элемента' />
            </div>
            <input type="hidden" name="Element[ingredients]" id="ingrInput" value="<?=$model->ingredients?>" />
            <div class="selected">
                <div class="selected-ids"><?= $model->ingredients ?></div>
                <div class="wrapper"></div>
                <div class="cc"></div>
            </div>
            <div class="all">
                <?php foreach($all as $element): ?>
                    <div class="AlkhimElement" id="all_AlkhimElement_<?=$element->id?>" 
                         data-name="<?=CHtml::encode($element->name)?>" data-img="<?=CHtml::encode($element->img)?>"
                         data-id="<?=CHtml::encode($element->id)?>">
                        <img src="<?=$element->img?>" alt="<?=CHtml::encode($element->name)?>" title="<?=CHtml::encode($element->name)?>"/>
                    </div>
                <?php endforeach; ?>
                <div class="cc"></div>
            </div>
        </div>
    </div>
    <div class="row submit">
        <?= CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить') ?>
    </div>
    <?= CHtml::endForm(); ?>
</div>