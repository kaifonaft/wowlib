<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error 404';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2>Error 404</h2>
<div class="error">
    <?php echo CHtml::encode($message); ?>
</div>