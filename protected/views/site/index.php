<div id="elements-grid">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
  'dataProvider' => $model->search(),
  'filter' => $model,
  'ajaxUpdate' => false,
  'columns' => array(
    array(
      'name' => 'id',
      'type' => 'text',
    ),
    array(
      'name' => 'name',
      'type' => 'html',
      'value' => array($model, 'gridGetRefName'),
    ),
    array(
      'name' => 'ingredients',
      'type' => 'html',
      'value'=> array($model, 'gridGetIngredients'),
    ),
    array(
      'name' => 'type_id',
      'type' => 'text',
      'value' => '$data->type->name',
      'filter' => CHtml::activeDropDownList($model, 'type_id', $model->getTypeListView(), array('prompt' => '-'))
    ),
    array(
      'name' => 'img',
      'value' => 'CHtml::image(Yii::app()->getBaseUrl().\'/\'.$data->img, "", array("class" => "element-img"))',
      'type' => 'raw',
      'filter' => false,
    ),
    array(
      'header' => 'Действия',
      'class' => 'MyButtonColumn'
    ),
  ),
));
?>
</div>