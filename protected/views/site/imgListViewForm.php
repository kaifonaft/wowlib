<?php /**
 * 
 */?>
<?= CHtml::scriptFile('js/ingrEditForm.js') ?>
<?php $selectedStr = CHtml::encode(implode(',', $selected)) ?>
<div id="ingredientSelector">
    <div>
        <input type="text" name='ingrSearch' id='ingrSearchInput' />
    </div>
    <input type="hidden" name="ingredients" id="ingrInput" value="<?=$selectedStr?>" />
    <div class="selected">
        <div class="selected-ids"><?= $selectedStr ?></div>
        <div class="wrapper"></div>
        <div class="cc"></div>
    </div>
    <div class="all">
        <?php foreach($all as $element): ?>
            <div class="AlkhimElement" id="all_AlkhimElement_<?=$element->id?>" 
                 data-name="<?=CHtml::encode($element->name)?>" data-img="<?=CHtml::encode($element->img)?>"
                 data-id="<?=CHtml::encode($element->id)?>">
                <img src="<?=$element->img?>" alt="<?=CHtml::encode($element->name)?>" title="<?=CHtml::encode($element->name)?>"/>
            </div>
        <?php endforeach; ?>
        <div class="cc"></div>
    </div>
</div>



