<?php

class SiteController extends Controller
{
    /**
     * Главная страница библиотеки рецептов
     */
    public function actionIndex(){
        $model = new Element('search');
        $model->unsetAttributes();
//        Log::trace($_POST, 'post');
//        Log::trace($_GET, 'get');
        if(isset($_GET['Element'])){
            if($_GET['Element']['id']){
                $model->id = $_GET['Element']['id'];
            }
            if($_GET['Element']['name']){
                $model->name = $_GET['Element']['name'];
            }
            if($_GET['Element']['type_id']){
                $model->type_id = $_GET['Element']['type_id'];
            }
            if($_GET['Element']['ingredients']){
                $model->ingredients = $_GET['Element']['ingredients'];
                if(is_string($model->ingredients)){
                    $model->ingredientsArray = explode(',', $model->ingredients);
                }else{
                    $model->ingredientsArray = array();
                }
            }
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Тестовая вьюшка
     */
    public function actionTest(){
        $res = Yii::app()->db->createCommand()
            ->select('id')
            ->from('element')
            ->queryColumn();
        Yii::trace(print_r($res,true));
        $this->render('empty');
    }
    
    public function actionListImg(){
        $all = Element::model()->findAll();
        $selected = array(13,3,3);
        $this->render('imgListViewForm', array(
          'all' => $all,
          'selected' => $selected,
        ));
    }

    /**
     * полное отображение алх. элемента
     * @param int $id id отображаемого элемента
     */
    public function actionView($id){
        $element = Element::model()->find('id = :id', array('id' => $id));
        if(is_null($element)){
            throw new CHttpException(404, 'Нет такого элемента');
        }
        $this->render('view', array(
          'model' => $element
        ));
    }
    
    /**
     * Страница добавления
     */
    public function actionAlkhimAdd(){
        $element = new Element();
        if(isset($_POST['Element'])){
            $element->attributes = $_POST['Element'];
            $element->imgFile = CUploadedFile::getInstance($element, 'imgFile');
            $saveSuccess = false;
            if($element->save()){
                $saveSuccess = true;
                $id = $element->getPrimaryKey();
                $element->saveImg();
                $this->redirect(array('site/AlkhimEdit', 'id' => $id));
                echo 'all cool';
            }
            if(!$saveSuccess){
                echo 'fail!';
            }
        }
        $all = Element::model()->findAll();
        $this->render('addedit', array(
          'model' => $element,
          'all' => $all,
        ));
    }
    
    /**
     * Удаление элемента
     */
    public function actionAlkhimDelete($id){
        $model = Element::model()->findByPk($id);
        if(!is_null($model)){
            if($model->delete()){
                $message = 'delete success';
            }else{
                $message = 'delete error';
            }
        }else{
            $message = 'error. element not exist';
        }
        $this->render('delete', array(
          'model' => $model,
          'message' => $message,
        ));
    }
    
    public function actionAlkhimEdit($id){
        $model = Element::model()->findByPk($id);
        if(is_null($model)){
            throw new CHttpException(404, 'Такого элемента не существует');
        }
        Yii::trace(print_r($_POST,true), 'application._POST');
        $message = '';
        if(isset($_POST['Element'])){
            $model->attributes = $_POST['Element'];
            if($model->save()){
                $fileDelete = $_POST['Element']['imgDelete'];
                $model->saveImg($fileDelete, $model->img);
                $message = 'save success';
            }else{
                $message = 'save error';
            }
        }
        $all = Element::model()->findAll();
        $this->render('addedit', array(
          'model' => $model,
          'message' => $message,
          'all' => $all,
        ));
    }
    
    public function actionError(){
        $error = Yii::app()->errorHandler->error;
        if($error){
            if(Yii::app()->request->isAjaxRequest){
                echo $error['message'];
            }else{
                if($error['code'] == 404){
                    $this->render('e404', $error);
                }else{
                    $this->render('error', $error);
                }
            }
        }
    }
    
    public function actionE404(){
        $this->render('e404');
    }
}