-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.41-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица wowlib.element
DROP TABLE IF EXISTS `element`;
CREATE TABLE IF NOT EXISTS `element` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название элемента',
  `type_id` int(11) NOT NULL COMMENT 'Тип элемента',
  `img` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  PRIMARY KEY (`id`),
  KEY `FK_element_type` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Элемент - рецепт, зелье, ингредиент ...';

-- Дамп данных таблицы wowlib.element: ~10 rows (приблизительно)
DELETE FROM `element`;
/*!40000 ALTER TABLE `element` DISABLE KEYS */;
INSERT INTO `element` (`id`, `name`, `type_id`, `img`) VALUES
	(3, 'Лиловый лотос', 2, 'images/3.jpg'),
	(4, 'Кровь грома', 2, 'images/4.jpg'),
	(5, 'Золотой клевер', 2, 'images/5.jpg'),
	(7, 'Солнечник', 2, 'images/7.jpg'),
	(10, '1233', 3, NULL),
	(12, 'Дикая лоза', 2, 'images/12.jpg'),
	(13, 'Огнецвет', 2, 'images/13.jpg'),
	(14, 'Эликсир великанов', 3, 'images/14.jpg'),
	(17, 'qqq', 1, NULL),
	(18, 'пр', 1, NULL);
/*!40000 ALTER TABLE `element` ENABLE KEYS */;


-- Дамп структуры для таблица wowlib.element_ingredients
DROP TABLE IF EXISTS `element_ingredients`;
CREATE TABLE IF NOT EXISTS `element_ingredients` (
  `recept_id` int(11) NOT NULL COMMENT 'id рецепта(таблица element)',
  `ingredient_id` int(11) NOT NULL COMMENT 'id ингредиента(таблица element)',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT 'количество этого ингредиента в рецепте',
  KEY `рецепт` (`recept_id`),
  KEY `ингредиенты` (`ingredient_id`),
  CONSTRAINT `ингредиенты` FOREIGN KEY (`ingredient_id`) REFERENCES `element` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `рецепт` FOREIGN KEY (`recept_id`) REFERENCES `element` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='задает ингредиенты для алхимического элемента(рецепта)';

-- Дамп данных таблицы wowlib.element_ingredients: ~14 rows (приблизительно)
DELETE FROM `element_ingredients`;
/*!40000 ALTER TABLE `element_ingredients` DISABLE KEYS */;
INSERT INTO `element_ingredients` (`recept_id`, `ingredient_id`, `count`) VALUES
	(14, 12, 1),
	(14, 4, 1),
	(7, 4, 2),
	(7, 3, 1),
	(7, 5, 1),
	(7, 10, 5),
	(17, 10, 1),
	(17, 4, 2),
	(17, 5, 2),
	(17, 7, 1),
	(17, 13, 1),
	(18, 17, 1),
	(18, 13, 2),
	(18, 12, 3);
/*!40000 ALTER TABLE `element_ingredients` ENABLE KEYS */;


-- Дамп структуры для таблица wowlib.type
DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название типа алхим. элемента',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Тип алхимического элемента(трава, зелье, ...)';

-- Дамп данных таблицы wowlib.type: ~3 rows (приблизительно)
DELETE FROM `type`;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` (`id`, `name`) VALUES
	(1, 'рыба'),
	(2, 'трава'),
	(3, 'зелье');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
